package com.mkozykowski.wsdl.service;

import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.xml.bind.JAXBElement;
import javax.xml.ws.soap.AddressingFeature;

import com.mkozykowski.wsdl.service.datacontract.ParametryWyszukiwania;
import com.mkozykowski.wsdl.service.tempuri.UslugaBIRzewnPubl;

public class GUSProjectMain {

	public static void main(String[] args) {
		final JPanel panel = new JPanel();
		UslugaBIRzewnPubl service = new UslugaBIRzewnPubl();
		service.setHandlerResolver(new SoapHandlerResolver()); // add sid to
																// header
		IUslugaBIRzewnPubl port = service.getE3(new AddressingFeature());
		String statusUslugi = "StatusUslugi";
		String result = (String) port.getValue(statusUslugi);
		java.util.logging.Logger.getLogger(GUSProjectMain.class.getName()).log(java.util.logging.Level.INFO,
				"Service status: " + result);
		if (result.trim().equals("0")) {
			JOptionPane.showMessageDialog(panel, "Usługa GUS niedostępna", "Błąd", JOptionPane.ERROR_MESSAGE);

		} else if (result.trim().equals("2")) {
			JOptionPane.showMessageDialog(panel, "Przerwa techniczna - usługa GUS niedostępna", "Błąd",
					JOptionPane.ERROR_MESSAGE);
		}

		/*
		 * logowanie, jezeli sesja wygasla, badz logowanie pierwszy raz
		 */
		if ((SoapMessageHandler.sessionCookie.equals("")) || (!result.equals("1"))) {
			String sid = port.zaloguj("abcde12345abcde12345");
			java.util.logging.Logger.getLogger(GUSProjectMain.class.getName()).log(java.util.logging.Level.INFO,
					"Recived session sid: " + sid);
			SoapMessageHandler.sessionCookie = sid;
		}
		System.out.println(port.danePobierzPelnyRaport("610188201", "PublDaneRaportPrawna"));

		/*
		 * // przykład wyszukiwania po NIPie ObjectFactory objectFactory = new
		 * ObjectFactory(); JAXBElement<String> nipParam =
		 * objectFactory.createParametryWyszukiwaniaNip("7740001454");
		 * ParametryWyszukiwania parametryWyszukiwania = new
		 * ParametryWyszukiwania(); parametryWyszukiwania.setNip(nipParam);
		 * String raport = port.daneSzukaj(parametryWyszukiwania);
		 */
	}

}
